﻿#include <iostream>
#include <string>
using namespace std;

int main()
{
    string str = "Hello World!";
    cout << "str="<<str<<endl;
    cout << "str.length()="<<str.length()<<endl;
    cout << "str[0]="<<str[0]<<endl;
    cout << "str[str.length()-1]="<<str[str.length()-1]<<endl;
}